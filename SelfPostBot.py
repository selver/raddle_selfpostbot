from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.chrome.options import Options
import datetime
import re


def login(driver, username, password):
    driver.get("http://www.raddle.me/login")

    elem = driver.find_element_by_id("login-username")
    elem.clear()
    elem.send_keys(username)

    elem = driver.find_element_by_id("login-password")
    elem.clear()
    elem.send_keys(password)

    elem.send_keys(Keys.RETURN)

    wait = WebDriverWait(driver, 30)
    wait.until(EC.title_is("Raddle"))

def sticky(driver):

    url = driver.current_url.split("/")
    driver.get("https://www.raddle.me/f/" + url[-3] + "/edit_submission/" + url[-2])


    elem = driver.find_element_by_id("submission_sticky")
    elem.click()

    elem = driver.find_element_by_id("submission_submit")
    elem.click()

def start_new_thread(driver, subraddle, thread_title, content):
    driver.get("http://www.raddle.me/submit/" + subraddle)

    wait = WebDriverWait(driver, 30)
    wait.until(EC.element_to_be_clickable((By.ID, "submission_title")))

    elem = driver.find_element_by_id("submission_title")
    elem.clear()
    elem.send_keys(thread_title)

    elem = driver.find_element_by_id("submission_body")
    elem.clear()
    elem.send_keys(content)

    elem = driver.find_element_by_id("submission_submit")
    elem.click()

    wait = WebDriverWait(driver, 30)
    wait.until(EC.title_is(thread_title))

    sticky(driver)

def get_bots(driver):
    driver.get("https://raddle.me/f/RaddleBots/27464/create-your-weekly-daily-self-post-bots-here")

    elements = driver.find_elements_by_css_selector('div.comment-body > p')
    bots = []
    for e in elements:
        text = re.findall('\[.*?\]',e.text)
        bot = {}
        for t in text:
            t = t.replace("[","")
            t = t.replace("]","")
            t = t.split(":")
            bot[t[0]] = t[1]
        bots.append(bot)
    return bots

def main():
    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument('--headless')
    chrome_options.add_argument('--no-sandbox')
    chrome_driver = "/home/selver/Projects/Chrome/chromedriver"
    driver = webdriver.Chrome(chrome_options=chrome_options, executable_path=chrome_driver)
    login(driver, "ThreadBot", "*********")

    bots = get_bots(driver)
    date = {"day":datetime.datetime.now().strftime('%a'),"time":datetime.datetime.now().strftime('%-I %p')}
    for b in bots:
        if date["day"] in b["Days"]:
            if date["time"] == b["Time"]:
                start_new_thread(driver, b["Subraddle"], b["Title"], b["Content"])

    driver.close()

if __name__ == "__main__":
    main()
